const request = require("request-promise");
const fs = require("fs");
const { exec, execSync } = require("child_process");
const { getToken } = require("./get-token");

async function post(markdown, id, token) {
  const options = {
    url: `${process.env.BASE_URL}/api/content`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    json: true,
    body: {
      ContentItemId: id,
      MarkdownBodyPart: {
        Markdown: markdown,
      },
    },
  };

  return await request(options);
}

(async () => {
  const files = fs.readdirSync("./");

  if (!files.includes("digital.statcan.gc.ca")) {
    execSync(
      `git clone https://oauth2:${process.env.ACCESS_TOKEN}@gitlab.com/stc-digital-innovation/ops/digital.statcan.gc.ca.git`
    );
  }

  // Get the file pointers
  const filePointers = fs.readdirSync("digital.statcan.gc.ca/open-source");

  console.log(`Obtaining token from ${process.env.BASE_URL}`);
  const token = await getToken(
    process.env.BASE_URL,
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET
  );

  console.log("Obtained token");

  for (let filePointer of filePointers) {
    const file = JSON.parse(
      fs.readFileSync(`digital.statcan.gc.ca/open-source/${filePointer}`)
    );
    const repoUrl = `https://oauth2:${process.env.ACCESS_TOKEN}@${file.repo_url}`;
    const repoName = file.repo_url.substring(
      file.repo_url.lastIndexOf("/") + 1,
      file.repo_url.lastIndexOf(".")
    );

    if (!files.includes(repoName)) {
      execSync(`git clone ${repoUrl}`);
    }

    let markdown = fs.readFileSync(`${repoName}/${file.file_path}`, {
      encoding: "utf-8",
    });

    markdown = markdown.replace(/---[\s\S]*---/, "");

    console.log(await post(markdown, file.contentId, token));
  }
})();
