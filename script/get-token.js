const request = require("request-promise");

async function getToken(baseUrl, clientId, clientSecret) {
  const options = {
    url: `${baseUrl}/connect/token`,
    method: "POST",
    headers: {
      Authorization: `Basic ${Buffer.from(
        `${clientId}:${clientSecret}`
      ).toString("base64")}`,
    },
    form: {
      grant_type: "client_credentials",
      scope: "openid profile roles",
    },
  };

  const result = JSON.parse(await request(options));

  return result.access_token;
}

module.exports = { getToken };
