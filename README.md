# Oc Api Demo

A simple client that interacts with oc through api.

# OC Configuration

## OpenID Settings

Features needs to be enabled:

- OpenID Authorization Server
- OpenID Token Valiadation

Enable client credentials flow then register the client app with the client id and secret.

## Role Permission

A role with api permissions needs to be created and assigned to the client app.

## OC Api Endpoint

The OC api endpoint is at /api/content/ for content management. This endpoint supports get, post and delete.

# Client

## Request Token

To request a token:

- Send a post request to /connect/token with `Authorization: base64(<client id>:<client secret>)`. The body(encoded as form body) needs to include `grant_type: client_credentials` and `scope: openid profile roles`.

## Request the api

To request the api include the token `Authorization: Bearer <token>` in every request.

# Demo Script

The demo script publishes a markdown file to oc.
